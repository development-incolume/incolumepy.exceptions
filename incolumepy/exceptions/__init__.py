"""
Principal Module.

Update metadata from version by semver
"""

from pathlib import Path

import toml

configfile = Path(__file__).parents[2].joinpath("pyproject.toml")
versionfile = Path(__file__).parent.joinpath("version.txt")
try:
    versionfile.write_text(f"{toml.load(configfile)['tool']['poetry']['version']}\n")
except FileNotFoundError:
    pass

__version__ = versionfile.read_text().strip()


class NonexequiException(Exception):
    """NonexequiException.

    Not exec flag exception.

    """

    def __init__(self, *args, **kwargs):
        """Class init."""
        super().__init__(*args)


if __name__ == "__main__":  # pragma: no cover
    print(__version__)
