# coding: utf-8
import re

import pytest

from incolumepy.exceptions import configfile,  __version__, NonexequiException


def test_pyproject_exist():
    assert configfile.is_file(), f"configfile: {configfile}"


@pytest.mark.fasttest
@pytest.mark.parametrize(
    "entrance",
    (
        __version__,
        "0.0.1",
        "0.1.0",
        "1.0.0",
        "1.0.1",
        "1.1.1",
        "1.0.1-dev.0",
        "1.0.1-dev.1",
        "1.0.1-dev.2",
        "1.0.1-alpha.0",
        "1.0.1-alpha.266",
        "1.0.1-dev.0",
        "1.0.1-beta.0",
    ),
)
def test_version(entrance):
    assert re.fullmatch(r"\d+(.\d+){2}(-\w+.\d+)?", entrance, flags=re.I)


def fakeraises(exc, msg: str):
    raise exc(msg)


@pytest.mark.parametrize(
    "entrance",
    (
        {"exc": NonexequiException, "msg": None},
        {"exc": NonexequiException, "msg": "Nok"},
        {"exc": NonexequiException, "msg": "Não executar"},
    ),
)
def test_nonexequi(entrance):
    with pytest.raises(entrance["exc"], match=entrance["msg"]):
        fakeraises(**entrance)
